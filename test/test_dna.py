import unittest
import json

from server import server
from models.abc import db
from models import User
from repositories import UserRepository


class TestUser(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.client = server.test_client()

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get(self):
        """ The GET on `/dna` should return a sequence """
        response = self.client.get('/application/dna/5')

        self.assertEqual(response.status_code, 200)
