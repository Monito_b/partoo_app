from flask import Flask, jsonify
import random
from flask_caching import Cache
from server import cache
from server import server

class Dna():
    """ The Dna model """
    sequence = ""

    def __init__(self, length):
        """ Create a new Dna """
        self.sequence =  ''.join(random.choice('CGTA') for _ in range(length))

    def getDna():
        return sequence
